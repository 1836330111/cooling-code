#include<stdio.h>

typedef unsigned char u8;
typedef unsigned int u32;

typedef struct as_cooling_temp {
    int tas;
    int tap;
} asCoolingTemp;

//散热表原始数据 
int cooling_code[]={
0x5a, 0xa5, 0x00, 0x01, 0x05, 0x12, 0x00, 0x64, 0x00, 0x76, 0x0a, 0x0a, 0xff, 0x20, 0x21, 0x22,
0x23, 0x24, 0xfb, 0x1e, 0xfb, 0x39, 0xfb, 0x39, 0xfb, 0x32, 0xfb, 0x3c, 0x23, 0x27, 0x3e, 0x46,
0x36, 0x3e, 0x30, 0x34, 0x3c, 0x41, 0x24, 0x28, 0x40, 0x48, 0x38, 0x40, 0x33, 0x37, 0x41, 0x46,
0x25, 0x29, 0x42, 0x4a, 0x3a, 0x42, 0x36, 0x3a, 0x46, 0x4b, 0x26, 0x2a, 0x44, 0x4c, 0x3c, 0x44,
0x38, 0x3c, 0x4b, 0x50, 0x27, 0x2b, 0x46, 0x4e, 0x3e, 0x46, 0x3a, 0x3e, 0x50, 0x55, 0x28, 0x2d,
0x48, 0x50, 0x40, 0x48, 0x3c, 0x40, 0x55, 0x5a, 0x29, 0x2f, 0x4a, 0x52, 0x42, 0x4a, 0x3e, 0x42,
0x5a, 0x64, 0x2a, 0x31, 0x4b, 0x53, 0x44, 0x4c, 0x40, 0x44, 0x5f, 0x69, 0x2b, 0x33, 0x4c, 0x54,
0x46, 0x4e, 0x42, 0x46, 0x64, 0x6e, 0x5a, 0xa5, 0x00, 0x02, 0x07, 0x14, 0x00, 0x46, 0x00, 0x5a,
0x07, 0x0a, 0xff, 0x30, 0x31, 0x32, 0x33, 0x34, 0x18, 0x19, 0x0a, 0x1e, 0x0a, 0x32, 0x32, 0x32,
0x32, 0x0f, 0x32, 0x1e, 0x3c, 0x3c, 0x3c, 0x3c, 0x0f, 0x46, 0x28, 0x46, 0x46, 0x46, 0x46, 0x14,
0x5a, 0x32, 0x50, 0x50, 0x50, 0x50, 0x1e, 0x64, 0x32, 0x5a, 0x5a, 0x5a, 0x5a, 0x28, 0x64, 0x3c,
0x64, 0x64, 0x64, 0x64, 0x32, 0x64, 0x46, 0x64, 0x64, 0x64, 0x64, 0x41, 0x64, 0x4b, 0x64, 0x64,
0x64, 0x64, 0x50, 0x64, 0x50, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64,
0x5a, 0xa5, 0x00, 0x03, 0x01, 0x0e, 0x00, 0x16, 0x00, 0x24, 0x02, 0x0b, 0xff, 0x25, 0xfb, 0x1e,
0x1f, 0x25, 0x22, 0x24, 0x23, 0x25, 0x24, 0x26, 0x25, 0x27, 0x26, 0x28, 0x28, 0x2a, 0x29, 0x2b,
0x2a, 0x2c, 0x2b, 0x2d}; 

void table_init(int cooling_code[]);

int main()
{
	table_init(cooling_code);
		}		
		
		
void table_init(int cooling_code[])
{
//tableclass1数据主体总数 

int table1_num=cooling_code[8]*256+cooling_code[9];
int table1_data_num= cooling_code[6]*256+cooling_code[7];
//tableclass2数据主体总数 
int table2_num= cooling_code[table1_num+8]*256+cooling_code[table1_num+9];
int table2_data_num= cooling_code[table1_num+6]*256+cooling_code[table1_num+7];
//tableclass1+tableclass2的和 
int table1_and_table2_num= table1_num+table2_num;
//tableclass3数据主体总数 
int table3_num= cooling_code[table1_and_table2_num+8]*256+cooling_code[table1_and_table2_num+9];
int table3_data_num= cooling_code[table1_and_table2_num+6]*256+cooling_code[table1_and_table2_num+7];

//接收tableclass1数据主体 
int get_temparture_level[table1_data_num];
int* p_get_temparture_level=&get_temparture_level;

//接收tableclass2数据主体 
int get_tempraturelevel_fanspeed[table2_num];
int* p_get_tempraturelevel_fanspeed=&get_tempraturelevel_fanspeed;

//接收tableclass3数据主体 
int get_env_temprature_level[table3_num];
int* p_get_env_temprature_level=&get_env_temprature_level;
//tableclass1数据初始化 
asCoolingTemp temparture_level[5][10];
//tableclass2数据初始化
int tempraturelevel_fanspeed[7][10];
//tableclass3数据初始化
asCoolingTemp env_temprature_level[1][11];
//for循环参数 
int m,n;
int i,j,k=0;

//table1
////将表格数据转成int 
for(m=0;m<table1_data_num;m++)
{
		if(cooling_code[m+cooling_code[5]]==0xfb){//int  32767
	    get_temparture_level[m]=(int)(cooling_code[m+cooling_code[5]]-256);
///		printf("get_cooling_fan_code:%d\n",get_cooling_fan_code[m]);//int 65536
		}
		else{
		get_temparture_level[m]=(int)cooling_code[m+cooling_code[5]];
//		printf("get_cooling_fan_code:%d\n",get_cooling_fan_code[m]);//int 65536
		}
}
//打印温度-等级表
for(n=0;n<table1_data_num;n++)
{
		if((n%10)==0)
		printf("\n");
		printf("  %d  ",get_temparture_level[n]);
}
printf("\n");



//初始化 数据表 
for(j=0;j<10;j++)
{
	for(i=0;i<5;i++)
	{
		temparture_level[i][j].tas=*(p_get_temparture_level+k);
		temparture_level[i][j].tap=*(p_get_temparture_level+k+1);
		k=k+2;
		printf("temparture_level[%d][%d].tas:%d  tap:%d\n",i,j,temparture_level[i][j].tas,temparture_level[i][j].tap);//int 65536

   }
   printf("\n");
 } 


//表格2 
//将表格数据转成int 
for(m=0;m<table2_data_num;m++)
{
		if(cooling_code[m+table1_num+cooling_code[table1_num+5]]==0xfb){//int  32767
	    get_tempraturelevel_fanspeed[m]=(int)(cooling_code[m+table1_num+cooling_code[table1_num+5]]-256);
//		printf("get_tempurelevel_fanspeed:%d\n",get_tempraturelevel_fanspeed[m]);//int 65536
		}
		else{
		get_tempraturelevel_fanspeed[m]=(int)cooling_code[m+table1_num+cooling_code[table1_num+5]];
//		printf("get_tempurelevel_fanspeed:%d\n",get_tempraturelevel_fanspeed[m]);//int 65536
		}
}
//打印温度-等级表
for(n=0;n<table2_data_num;n++)
{
		if((n%10)==0)
		printf("\n");
		printf("   %d   ",get_tempraturelevel_fanspeed[n]);
}
printf("\n");


//初始化 数据表 
k=0;
for(j=0;j<10;j++)
{
	for(i=0;i<7;i++)
	{
		tempraturelevel_fanspeed[i][j]=*(p_get_tempraturelevel_fanspeed+k);
		k++;
		printf("tempraturelevel_fanspeed[%d][%d]=%d\n",i,j,tempraturelevel_fanspeed[i][j]);//int 65536

   }
   printf("\n");
 } 


//table3
//将表格数据转成int 
for(m=0;m<table3_data_num;m++)
{
		if(cooling_code[m+table1_and_table2_num+cooling_code[table1_and_table2_num+5]]==0xfb){//int  32767
	    get_env_temprature_level[m]=(int)(cooling_code[m+table1_and_table2_num+cooling_code[table1_and_table2_num+5]]-256);
//		printf("get_env_temprature_level:%d\n",get_env_temprature_level[m]);//int 65536
		}
		else{
		get_env_temprature_level[m]=(int)cooling_code[m+table1_and_table2_num+cooling_code[table1_and_table2_num+5]];
//		printf("get_env_temprature_level:%d\n",get_env_temprature_level[m]);//int 65536
		}
}
//打印温度-等级表
for(n=0;n<table3_data_num;n++)
{
		if((n%11)==0)
		printf("\n");
		printf("  %d  ",get_env_temprature_level[n]);
}
printf("\n");


k=0;
//初始化 数据表 
for(j=0;j<11;j++)
{
	for(i=0;i<1;i++)
	{
		env_temprature_level[i][j].tas=*(p_get_env_temprature_level+k);
		env_temprature_level[i][j].tap=*(p_get_env_temprature_level+k+1);
		k=k+2;
		printf("env_temprature_level[%d][%d].tas:%d  tap:%d\n",i,j,env_temprature_level[i][j].tas,env_temprature_level[i][j].tap);//int 65536

   }
   printf("\n");
 } 
}


////将表格数据转成int 
//for(m=0;m<table1_data_num;m++)
//{
//		if(cooling_code[m+cooling_code[5]]==0xfb){//int  32767
//	    get_cooling_fan_code[m]=(int)(cooling_code[m+cooling_code[5]]-256);
/////		printf("get_cooling_fan_code:%d\n",get_cooling_fan_code[m]);//int 65536
//		}
//		else{
//		get_cooling_fan_code[m]=(int)cooling_code[m+cooling_code[5]];
////		printf("get_cooling_fan_code:%d\n",get_cooling_fan_code[m]);//int 65536
//		}
//}
////打印温度-等级表
//for(n=0;n<table1_data_num;n++)
//{
//		if((n%10)==0)
//		printf("\n");
//		printf("  %d  ",get_cooling_fan_code[n]);
//}
//printf("\n");
//
//
//
////初始化 数据表 
//for(j=0;j<10;j++)
//{
//	for(i=0;i<5;i++)
//	{
//		cooling_fan_code[i][j].tas=*(p_get_cooling_fan_code+k);
//		cooling_fan_code[i][j].tap=*(p_get_cooling_fan_code+k+1);
//		k=k+2;
//		printf("get_cooling_fan_code[%d][%d].tas:%d  tap:%d\n",i,j,cooling_fan_code[i][j].tas,cooling_fan_code[i][j].tap);//int 65536
//
//   }
//   printf("\n");
// }  

//表格2 
////将表格数据转成int 
//for(m=0;m<table2_data_num;m++)
//{
//		if(cooling_code[m+table1_num+cooling_code[table1_num+5]]==0xfb){//int  32767
//	    get_tempurelevel_fanspeed[m]=(int)(cooling_code[m+table1_num+cooling_code[table1_num+5]]-256);
//		printf("get_tempurelevel_fanspeed:%d\n",cooling_code[m+table1_num+cooling_code[table1_num+5]]);//int 65536
//		}
//		else{
//		get_tempurelevel_fanspeed[m]=(int)cooling_code[m+table1_num+cooling_code[table1_num+5]];
//		printf("get_tempurelevel_fanspeed:%d\n",cooling_code[m+table1_num+cooling_code[table1_num+5]]);//int 65536
//		}
//}
////打印温度-等级表
//for(n=0;n<table2_data_num;n++)
//{
//		if((n%10)==0)
//		printf("\n");
//		printf("  %d  ",get_tempurelevel_fanspeed[n]);
//}
//printf("\n");
//
//
//int tempurelevel_fanspeed[7][10];
//
////初始化 数据表 

//for(j=0;j<10;j++)
//{
//	for(i=0;i<7;i++)
//	{
//		tempurelevel_fanspeed[i][j]=*(p_get_tempurelevel_fanspeed+k);
//		k++;
//		printf("get_cooling_fan_code[%d][%d]=%d\n",i,j,tempurelevel_fanspeed[i][j]);//int 65536
//
//   }
//   printf("\n");
// } 
