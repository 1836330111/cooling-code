# coding=utf-8
import sys
import os
import xlrd
import numpy as np
import pandas as pd

 #！《表格在excel中的位置要从第二行开始》很重要
#ab+ 以二进制格式打开一个文件，如果文件已经有了，就续上去
def bin_save_file(file_path,bin_arry,md = 'ab+'):
    fp = open(file_path,md)
    fp.write(bin_arry)
    fp.close()

io=r'/home/he/python_study/L8_new_cooling/L8_newcooling.xlsx' #散热逻辑excel的路径
# sheet=pd.read_excel(io,sheet_name = '温度-等级表')                      #可以通过sheet_name来指定读取的表单
# sheet2=pd.read_excel(io,sheet_name = '温度等级-风扇转速')          #可以通过sheet_name来指定读取的表单
def print_sheet_info(sheet,table_class):
    global row_num
    global col_num
    global rows1
    global cols1
    row_num=sheet.shape[0]#获取sheet1行数
    col_num=sheet.shape[1]#获取sheet1列数
    #print(sheet) #打印整个sheet1的表格
    print(" sheet%d为%d行，%d列的表格。\n"%(table_class,sheet.shape[0],sheet.shape[1]))#打印表格总行、列数信息
    #！《表格在excel中的位置要从第二行开始》很重要
    rows1=sheet.iloc[0,:]#0表示第一行 这里读取数据并不包含表头，要注意哦！
    #print(rows1)
    cols1=sheet.iloc[:, 0]#第一列
   # print(cols1)
    #col=sheet.iloc[x:y]#x表示行数，y表示列数，获取莫一行或者列所有数据用    ：
    return row_num,col_num

def get_usenum_row_col(sheet,row_num,col_num):
    global unuse_row_num
    global use_row_num
    use_row_num_arr=bytearray(col_num)
    #有效行数，《整个表格除去汉字描述的行》
    #判断所有列有效行数有多少
    for j in range(col_num):
        for i in range(row_num):
             if((type(sheet.iloc[:, j].values[i]) is int) or(type(sheet.iloc[:, j].values[i]) is float)):
                use_row_num_arr[j]=use_row_num_arr[j]+1
    use_row_num=min(use_row_num_arr)

    unuse_row_num=row_num-use_row_num #得到无效的行数，《汉字描述的行》
    print(" 有效行数：%d\n"%use_row_num)
    print(" 无效行数：%d\n"%unuse_row_num)
    return use_row_num

#生成5A A5的协议头
def make_bin_head_from_list_row_colums(sheet,row_num,col_num,table_class):
    bin_data_num=  ((col_num-1)*get_usenum_row_col(sheet,row_num,col_num))
    bin_buf_head = bytearray(50)
    #sync
    bin_buf_head[0] = 0x5a
    bin_buf_head[1] = 0xa5
    #class
    bin_buf_head[2] = 0x00
    bin_buf_head[3] = table_class
    #check
    bin_buf_head[4] = 0x00#表格头部长度
    bin_buf_head[5] = 0x00 #bin_buf_head长度
    bin_buf_head[6] = (bin_data_num& 0xFF00) >> 8#数据buf高八位
    bin_buf_head[7] = (bin_data_num& 0x00FF) #数据buf低八位
    bin_buf_head[8] = 0x00
    bin_buf_head[9] = 0x00
    bin_buf_head[10]=col_num-1#列
    bin_buf_head[11]=get_usenum_row_col(sheet,row_num,col_num)  #行数
    bin_buf_head[12] = 0xff 
    index= 13
#获取相应的表头信息
    for x in sheet.iloc[0,:]:
        if 'FAN1' == x :
            bin_buf_head[index] = 0x00
            index= index+1
        elif 'FAN2' == x:
            bin_buf_head[index] = 0x01
            index= index+1
        elif 'FAN3' == x:
            bin_buf_head[index] = 0x02
            index= index+1
        elif 'FAN4' == x:
            bin_buf_head[index] = 0x03
            index= index+1
        elif 'FAN5' == x:
            bin_buf_head[index] = 0x04
            index= index+1
        elif 'FAN6' == x:
            bin_buf_head[index] = 0x05
            index= index+1
        elif 'FAN7' == x:
            bin_buf_head[index] = 0x06
            index= index+1
        elif 'FAN8' == x:
            bin_buf_head[index] = 0x07
            index= index+1
        elif 'FAN9' == x:
            bin_buf_head[index] = 0x08
            index= index+1
        elif 'FAN10' == x:
            bin_buf_head[index] = 0x09
            index= index+1
        elif 'FAN11' == x:
            bin_buf_head[index] = 0x0A
            index= index+1
        elif 'FAN12' == x:
            bin_buf_head[index] = 0x0B
            index= index+1
        elif 'FAN13' == x:
            bin_buf_head[index] = 0x0C
            index= index+1
        elif 'FAN14' == x:
            bin_buf_head[index] = 0x0D
            index= index+1
        elif 'FAN15' == x:
            bin_buf_head[index] = 0x0E
            index= index+1
        elif 'FAN16' == x:
            bin_buf_head[index] = 0x0F
            index= index+1
        elif 'FAN17' == x:
            bin_buf_head[index] = 0x10
            index= index+1
        elif 'FAN18' == x:
            bin_buf_head[index] = 0x11
            index= index+1
        elif 'FAN19' == x:
            bin_buf_head[index] = 0x12
            index= index+1
        elif 'FAN20' == x:
            bin_buf_head[index] = 0x13
            index= index+1
        elif 'FAN21' == x:
            bin_buf_head[index] = 0x14
            index= index+1
        elif 'FAN22' == x:
            bin_buf_head[index] = 0x15
            index= index+1
        elif 'FAN23' == x:
            bin_buf_head[index] = 0x16
            index= index+1
        elif 'FAN24' == x:
            bin_buf_head[index] = 0x17
            index= index+1
        elif 'FAN_25' == x:
            bin_buf_head[index] = 0x18
            index= index+1
        elif 'FAN_26' == x:
            bin_buf_head[index] = 0x19
            index= index+1
        elif 'FAN27' == x:
            bin_buf_head[index] = 0x1A
            index= index+1
           
        elif '棱镜温度' == x:
            bin_buf_head[index] = 0x20
            index= index+1
            #print(x, end = ' ')
        elif '色轮温度' == x:
            bin_buf_head[index] = 0x21
            index= index+1
            #print(x, end = ' ')
        elif '光源温度' == x:
            bin_buf_head[index] = 0x22
            index= index+1
            #print(x, end = ' ')       
        elif 'DMD热面温度' == x:
            bin_buf_head[index] = 0x23
            index= index+1
            #print(x, end = ' ')
        elif '电源温度' == x:
            bin_buf_head[index] = 0x24
            index= index+1
            #print(x, end = ' ')   2
        elif 'NTC13环温' == x:
            bin_buf_head[index] = 0x25
            index= index+1
            #print(x, end = ' ')   2

        elif 'FAN_1_8' == x:
            bin_buf_head[index] = 0x30
            index= index+1
        elif 'FAN_9_12' == x:
            bin_buf_head[index] = 0x31
            index= index+1
        elif 'FAN_13_18' == x:
            bin_buf_head[index] = 0x32
            index= index+1
        elif 'FAN_19_20' == x:
            bin_buf_head[index] = 0x33
            index= index+1
        elif 'FAN_21_24_27' == x:
            bin_buf_head[index] = 0x34
            index= index+1
        
    bin_buf_head[4] = index-13
    bin_buf_head[5] = index

    bin_buf_head[8] = ((bin_data_num+index)& 0xFF00) >> 8#数据buf高八位
    bin_buf_head[9] = ((bin_data_num+index)& 0x00FF) #数据buf低八位
    #print( (bin_buf_head[0:index]))        
    return bin_buf_head[0:index] 

#根据表生成数据主体
def make_bin_data_from_list_row_colums(sheet,row_num,col_num,table_class):
    bin_buf_data = bytearray(1000)
    index = 0
    for  x in  range(unuse_row_num,row_num):
            for y in range(1,col_num):
                if table_class ==0x01:
                    temp = sheet.iloc[x, y]
                    temp=temp%256    #这句话非常重要，用于当数据中出现负数的情况下例如：  -5->0xfb
                    bin_buf_data[index] =temp
                    index = index +1
                elif table_class ==0x02:
                    temp = sheet.iloc[x, y]
                    temp=int(temp*100)  #这句话非常重要，用于当数据中出现负数的情况下例如：  -5->0xfb
                    bin_buf_data[index] =temp
                    index = index +1
                elif table_class ==0x03:
                    temp = sheet.iloc[x, y]
                    temp=temp%256    #这句话非常重要，用于当数据中出现负数的情况下例如：  -5->0xfb
                    bin_buf_data[index] =temp
                    index = index +1
  #  print(bin_buf_data[0:index])
    return bin_buf_data[0:index]



def make_table_bin(sheet,table_class ):
        #生成表头的BIN
        col_num,col_num = print_sheet_info(sheet,table_class)
        head_buf =  make_bin_head_from_list_row_colums(sheet,row_num,col_num,table_class)

        #生成数据结构BIN
        head_data = make_bin_data_from_list_row_colums(sheet,row_num,col_num,table_class)

        #print("head_buf+head_data\n")
       # print(head_buf+head_data)

        # bin_save_file(r'ttll.bin',head_buf+head_data,'wb+')

        return head_buf+head_data

def make_bin( ):
        sheet=pd.read_excel(io,sheet_name = '温度-等级表')   #可以通过sheet_name来指定读取的表单
        res_bin = make_table_bin(sheet,1)
        bin_save_file(r'qq.bin',res_bin,'wb+')# wb+ 以二进制格式打开一个文件用于读写

        sheet=pd.read_excel(io,sheet_name = '温度等级-风扇转速')   #可以通过sheet_name来指定读取的表单
        res_bin = make_table_bin(sheet,2)
        bin_save_file(r'qq.bin',res_bin,'ab+')

        sheet=pd.read_excel(io,sheet_name = '新电流控制表')   #可以通过sheet_name来指定读取的表单
        res_bin = make_table_bin(sheet,3)
        bin_save_file(r'qq.bin',res_bin,'ab+')

make_bin( )
#if __name__ == '__main__':

#     ls = find_filename_by_ext('.','xlsx')
    
#     for x in ls:
#         #show_excel_info(x)
#         workbook = xlrd.open_workbook(x, encoding_override='gb18030')
#         #make_bin(workbook)
 #   os.system("bin2hextab.exe qq.bin")